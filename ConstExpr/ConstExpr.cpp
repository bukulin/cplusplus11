#include "ConstExpr.h"

#include <iostream>

using namespace std;

static constexpr int get_five()
{
	return 5;
}

static constexpr double earth_gravitation_acceleration = 9.8;
static constexpr double moon_gravitation_acceleration = earth_gravitation_acceleration / 6.0;


ConstExpr::ConstExpr()
	: Module("ConstExpr")
{
}

int ConstExpr::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	int array[7 + get_five()];

	cout << "Size of array is: "
	     << sizeof(array) / sizeof(int)
	     << endl;

	cout << "\nGravitation acceleration on the Earth: " << earth_gravitation_acceleration << endl
	     << "on the Moon: " << moon_gravitation_acceleration << endl;

	return 0;
}
