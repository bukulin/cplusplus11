// -*- mode: c++ -*-
#ifndef Constexpr_h
#define Constexpr_h 1

#include "Module.h"

class ConstExpr : public Module
{
public:
	ConstExpr();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
