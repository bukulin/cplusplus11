#include "TypeInference.h"

#include <iostream>
#include <vector>

using namespace std;

TypeInference::TypeInference()
	: Module("TypeInference")
{
}

int TypeInference::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	auto variable = 5;
	cout << "Variable:" << variable
	     << " size:" << sizeof(variable) << endl;

	decltype(variable) otherVariable = 6;

	cout << endl << "Other variable:" << otherVariable
	     << " size:" << sizeof(otherVariable) << endl;

	vector<int> v{1,2,3,4};
	cout << endl << "Vector elements by auto iterator type: ";
	for(auto each = v.cbegin(); each != v.cend(); ++each)
	{
		cout << *each << " ";
	}
	cout << endl;

	return 0;
}


