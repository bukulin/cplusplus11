// -*- mode: c++ -*-
#ifndef TypeInference_h
#define TypeInference_h 1

#include "Module.h"

class TypeInference : public Module
{
public:
	TypeInference();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
