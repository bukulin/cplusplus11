// -*- mode: c++ -*-
#ifndef Module_h
#define Module_h 1

#include <string>

class Module
{
protected:
	Module(const std::string& name);
public:
	virtual ~Module();

public:
	virtual int Main(int argc, char** argv);

private:
	const std::string name;
};

#endif // Module_h
