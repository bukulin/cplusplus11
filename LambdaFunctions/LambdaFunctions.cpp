#include "LambdaFunctions.h"

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

LambdaFunctions::LambdaFunctions()
	: Module("LambdaFunctions")
{
}

int LambdaFunctions::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	vector< int > v{1,2,3,4};
	int acc = 0;
	for_each( v.cbegin(),
		  v.cend(),
		  [&acc](int x){ acc += x; } );
	cout << "Sum of vector is: " << acc << endl;

	// With alternative function syntax
	acc = 0;
	for_each( v.cbegin(),
		  v.cend(),
		  [=, &acc](int x) -> int { acc += x; return acc; } );
	cout << "Sum of vector is: " << acc << endl;

	return 0;
}
