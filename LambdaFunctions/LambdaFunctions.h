// -*- mode: c++ -*-
#ifndef Lambdafunctions_h
#define Lambdafunctions_h 1

#include "Module.h"

class LambdaFunctions : public Module
{
public:
	LambdaFunctions();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
