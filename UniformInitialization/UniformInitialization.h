// -*- mode: c++ -*-
#ifndef UniformInitialization_h
#define UniformInitialization_h 1

#include "Module.h"

class UniformInitialization : public Module
{
public:
	UniformInitialization();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
