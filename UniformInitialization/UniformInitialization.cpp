#include "UniformInitialization.h"

#include <iostream>

using namespace std;

struct BasicStruct
{
	int x;
	double y;
};

struct AltStruct
{
public:
	AltStruct(int aX, double aY)
		: x{aX}
		, y{aY}
	{}

public:
	int x;
	double y;
};


UniformInitialization::UniformInitialization()
	: Module("UniformInitialization")
{
}

int UniformInitialization::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	BasicStruct b{1, 3.2};
	AltStruct a{3, 3.5};

	cout << "Basic structure: " << b.x << " " << b.y << endl;
	cout << "Alt structure: " << a.x << " " << a.y << endl;

	return 0;
}
