#include "OverrideFinal.h"
#include "Classes.h"

#include <iostream>

using namespace std;

OverrideFinal::OverrideFinal()
	: Module("OverrideFinal")
{
}
int OverrideFinal::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	Base b;
	cout << "Base.function(float): ";
	b.function(1.0f);

	Derived d;
	cout << "Derived.function(float): ";
	d.function(2.0f);

	return 0;
}
