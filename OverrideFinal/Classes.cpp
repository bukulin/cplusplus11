#include "Classes.h"

#include <iostream>

using namespace std;

Base::~Base()
{
}

void Base::function(float value)
{
	cout << __PRETTY_FUNCTION__ << " " << value << endl;
}

void Base::function(double value)
{
	cout << __PRETTY_FUNCTION__ << " " << value << endl;
}


Derived::Derived()
	: Base()
{
}

void Derived::function(float value)
{
	cout << __PRETTY_FUNCTION__ << " " << value << endl;
}
