// -*- mode: c++ -*-
#ifndef Classes_h_
#define Classes_h_ 1

class Base
{
public:
	Base() = default;
	virtual ~Base();

public:
	virtual void function(float value); // Function to override.
	virtual void function(double value) final; // Prevent subclasses to use this function prototype.
};

class Derived : public Base
{
public:
	Derived();

public:
	virtual void function(float value) override; // Overriding a function from the base class
	// virtual void function(int value) override;   // Compiler error:
						     // 'function' marked 'override' but
						     // does not override any member
						     // functions
};

#endif
