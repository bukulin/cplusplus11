// -*- mode: c++ -*-
#ifndef Overridefinal_h_
#define Overridefinal_h_ 1

#include "Module.h"

class OverrideFinal : public Module
{
public:
	OverrideFinal();
public:
	virtual int Main(int argc, char** argv) override;
};

#endif
