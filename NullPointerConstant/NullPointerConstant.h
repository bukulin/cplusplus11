// -*- mode: c++ -*-
#ifndef NullPointerConstant_h
#define NullPointerConstant_h 1

#include "Module.h"

class NullPointerConstant : public Module
{
public:
	NullPointerConstant();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
