#include "NullPointerConstant.h"

#include <iostream>

using namespace std;

static void foo(int arg)
{
	cout << __PRETTY_FUNCTION__ << endl;
	cout << "Argument is an integer type: "
	     << arg << endl;
}

static void foo(char* /* arg */)
{
	cout << __PRETTY_FUNCTION__ << endl;
	cout << "Argument is a character pointer type." << endl;
}

NullPointerConstant::NullPointerConstant()
	: Module("Null Pointer Constant")
{
}

int NullPointerConstant::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	int intArg = 0;
	foo(intArg);

	cout << endl;
	char* pointerArg = nullptr;
	foo(pointerArg);

	return 0;
}

