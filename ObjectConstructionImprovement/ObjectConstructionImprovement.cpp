#include "ObjectConstructionImprovement.h"
#include "Container.h"
#include "AlternativeDefaultValueSyntax.h"

#include <iostream>

using namespace std;

ObjectConstructionImprovement::ObjectConstructionImprovement()
	: Module("ObjectConstructionImprovement")
{
}

int ObjectConstructionImprovement::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	Container c1;
	cout << "Container value: " << c1.Value() << endl;

	Container c2(33);
	cout << "Container value: " << c2.Value() << endl;

	cout << endl;
	BaseClass b1;
	cout << "Base class value: " << b1.value << endl;
	BaseClass b2(22);
	cout << "Base class value: " << b2.value << endl;

	return 0;
}
