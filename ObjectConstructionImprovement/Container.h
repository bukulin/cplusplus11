// -*- mode: c++ -*-
#ifndef Container_h
#define Container_h 1

class Container
{
public:
	Container(const int& number);
	Container();

public:
	const int& Value() const;

private:
	const int number;
};

#endif
