#include "AlternativeDefaultValueSyntax.h"

#include <iostream>

using namespace std;

BaseClass::BaseClass(int aValue)
	: value(aValue)
{
	cout << __PRETTY_FUNCTION__ << endl;
}
BaseClass::BaseClass()
{
	cout << __PRETTY_FUNCTION__ << endl;
}

BaseClass::~BaseClass()
{}
