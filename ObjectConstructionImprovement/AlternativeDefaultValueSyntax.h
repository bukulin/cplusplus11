// -*- mode: c++ -*-
#ifndef Explicityinheritedconstructors_h
#define Explicityinheritedconstructors_h 1

class BaseClass
{
public:
	explicit BaseClass(int value);
	BaseClass();
	virtual ~BaseClass();

public:
	int value = 44;
};

#endif
