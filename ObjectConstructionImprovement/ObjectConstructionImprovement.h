// -*- mode: c++ -*-
#ifndef Objectconstructionimprovement_h
#define Objectconstructionimprovement_h 1

#include "Module.h"

class ObjectConstructionImprovement : public Module
{
public:
	ObjectConstructionImprovement();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
