#include "Container.h"

#include <iostream>

using namespace std;

Container::Container(const int& aNumber)
	: number(aNumber)
{
	cout << "Constructor with argument" << endl;
}
Container::Container()
	: Container(42)
{
	cout << "Constructor with no argument" << endl;
}

const int& Container::Value() const
{
	return number;
}
