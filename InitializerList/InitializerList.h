// -*- mode: c++ -*-
#ifndef InitializerList_h
#define InitializerList_h 1

#include "Module.h"

class InitializerList : public Module
{
public:
	InitializerList();

public:
	virtual int Main(int argc, char** argv) override;
};


#endif
