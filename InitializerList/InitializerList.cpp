#include "InitializerList.h"

#include <iostream>
#include <vector>

using namespace std;

struct Point
{
	int x;
	int y;
};

static ostream& operator << (ostream& os, const Point& p)
{
	os << "[" << p.x << "," << p.y << "]";
	return os;
}

InitializerList::InitializerList()
	: Module("InitializerList")
{
}

int InitializerList::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	Point p = {1,2};
	cout << "Point: " << p << "\n";

	vector< Point > points =
		{ {1,2}, {3,4} };

	cout << "\nVector: [";
	for(Point const& each: points)
	{
		cout << each;
	}
	cout << "]\n";

	initializer_list< string > StringInitializerList = {"Bilbo",
							    "Bofur",
							    "Bombur"};
	vector< string > names(StringInitializerList);
	cout << "\nNames: ";
	for(string const& name: names)
		cout << name << " ";
	cout << endl;

	return 0;
}
