#include "Module.h"

#include <iostream>

using namespace std;

Module::Module(const std::string& aName)
	: name( std::move(aName) )
{
}
Module::~Module()
{
}

int Module::Main(int /* argc */, char** /* argv */)
{
	cout << "\n\n========= "
	     << name
	     << " ========\n";

	return 0;
}
