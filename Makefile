SHELL = /bin/bash

TARGET = NewCppExamples

SOURCES = \
	main.cpp \
	Module.cpp \
	MoveConstructor/MoveConstructor.cpp \
	MoveConstructor/President.cpp \
	ConstExpr/ConstExpr.cpp \
	ExternalTemplate/ExternalTemplate.cpp \
	ExternalTemplate/Sum.cpp \
	ExternalTemplate/Prod.cpp \
	InitializerList/InitializerList.cpp \
	UniformInitialization/UniformInitialization.cpp \
	TypeInference/TypeInference.cpp \
	RangeBasedForLoop/RangeBasedForLoop.cpp \
	LambdaFunctions/LambdaFunctions.cpp \
	AlternativeFunctionSyntax/AlternativeFunctionSyntax.cpp \
	ObjectConstructionImprovement/ObjectConstructionImprovement.cpp \
	ObjectConstructionImprovement/Container.cpp \
	ObjectConstructionImprovement/AlternativeDefaultValueSyntax.cpp \
	OverrideFinal/OverrideFinal.cpp \
	OverrideFinal/Classes.cpp \
	NullPointerConstant/NullPointerConstant.cpp \
	StronglyTypedEnums/StronglyTypedEnums.cpp

CXX = clang++
LD = clang++
CPP = clang++

STD_FLAGS = -std=c++11
WARNING_FLAGS = -Werror -Weverything -pedantic -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic
DEBUG_FLAGS = -O0 -ggdb
INCLUDE_FLAGS = -I$(PWD)

CXXFLAGS = \
	$(STD_FLAGS) \
	$(WARNING_FLAGS) \
	$(DEBUG_FLAGS) \
	$(INCLUDE_FLAGS)
LDFLAGS =

OBJECTS = ${SOURCES:.cpp=.o}
DEPS = ${SOURCES:.cpp=.d}

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<
	@$(CPP) -MP -MG -MM -MF $*.d $<

.PHONY: clean
clean:
	-rm -rf \
		$(TARGET) \
		$(OBJECTS) \
		$(DEPS)
