// -*- mode: c++ -*-
#ifndef ExternalTemplate_h
#define ExternalTemplate_h 1

#include "Module.h"

class ExternalTemplate : public Module
{
public:
	ExternalTemplate();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
