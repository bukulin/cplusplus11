#include "Functions.h"

extern template class std::vector<int>; // Vector is not instantiated here.

int Sum(const std::vector<int>& vector)
{
	int acc = 0;
	for(int const& each: vector)
	{
		acc += each;
	}
	return acc;
}
