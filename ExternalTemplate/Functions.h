// -*- mode: c++ -*-
#ifndef Functions_h
#define Functions_h 1

#include <vector>

int Sum(const std::vector<int>& vector);
int Prod(const std::vector<int>& vector);

#endif
