#include "Functions.h"

extern template class std::vector<int>; // Vector is not instantiated here.

int Prod(const std::vector<int>& vector)
{
	int acc = 1;
	for(int const& each: vector)
	{
		acc *= each;
	}
	return acc;
}

