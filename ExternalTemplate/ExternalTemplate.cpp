#include "ExternalTemplate.h"
#include <iostream>

#include "Functions.h"

using namespace std;

template class std::vector<int>;

ExternalTemplate::ExternalTemplate()
	: Module("ExternalTemplate")
{
}

int ExternalTemplate::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	vector<int> v{1,2,3,4};
	cout << "Sum of vector: " << Sum(v) << endl;
	cout << "Prod of vector: " << Prod(v) << endl;

	return 0;
}
