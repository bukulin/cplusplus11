// -*- mode: c++ -*-
#ifndef Alternativefunctionsyntax_h
#define Alternativefunctionsyntax_h 1

#include "Module.h"

class AlternativeFunctionSyntax : public Module
{
public:
	AlternativeFunctionSyntax();
public:
	virtual int Main(int argc, char** argv) override;
};

#endif
