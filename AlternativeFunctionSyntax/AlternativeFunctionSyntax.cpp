#include "AlternativeFunctionSyntax.h"

#include <iostream>

using namespace std;

static auto Add(const int& x, const int& y) -> int
{
	return x+y;
}

template< class Lhs, class Rhs >
auto AddT(const Lhs& lhs, const Rhs& rhs) -> decltype(lhs + rhs)
{
	return lhs + rhs;
}

AlternativeFunctionSyntax::AlternativeFunctionSyntax()
	: Module("AlternativeFunctionSyntax")
{
}


int AlternativeFunctionSyntax::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	cout << "Sum of (3, 4): " << Add(3,4) << endl;
	cout << "Sum of (3, 3.14): " << AddT(3, 3.14) << endl;

	return 0;
}


