// -*- mode: c++ -*-
#ifndef President_h
#define President_h 1

#include <string>

class President
{
public:
	President() = default;
	President(const std::string& aName,
		  const std::string& aCountry,
		  const int& aYear );
	President(President&& rhs);

public:
	President& operator=(const President& rhs) = default;

public:
	std::string name;
	std::string country;
	int year;
};

#endif
