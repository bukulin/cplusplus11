#include "President.h"

#include <iostream>

President::President(const std::string& aName,
		     const std::string& aCountry,
		     const int& aYear )
	: name( std::move(aName) )
	, country( std::move(aCountry) )
	, year( aYear )
{
	std::cout << "I am being constructed\n";
}

President::President(President&& rhs)
	: name( std::move(rhs.name) )
	, country( std::move(rhs.country) )
	, year( rhs.year )
{
	std::cout << "I am being moved\n";
}

