// -*- mode: c++ -*-
#ifndef MoveConstructor_h
#define MoveConstructor_h 1

#include "Module.h"

class MoveConstructor : public Module
{
public:
	MoveConstructor();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
