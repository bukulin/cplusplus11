#include "MoveConstructor.h"
#include "President.h"

#include <iostream>
#include <vector>

using namespace std;

MoveConstructor::MoveConstructor()
	: Module("MoveConstructor")
{
}

int MoveConstructor::Main(int argc, char** argv )
{
	Module::Main(argc, argv);

	vector< President > elections;
	cout << "emplace_back:\n";
	elections.emplace_back("Nelson Mandela", "South Africa", 1994);

	vector< President > reElections;
	cout << "\npush_back:\n";
	reElections.push_back(
		President("Franklin Delano Roosevelt", "USA", 1936) );

	cout << "\nContents:\n";
	for(President const& president: elections)
	{
		cout << president.name << " was elected president of "
		     << president.country << " in "
		     << president.year << ".\n";
	}

	for(President const& president: reElections)
	{
		cout << president.name << " was re-elected president of "
		     << president.country << " in "
		     << president.year << ".\n";
	}

	return 0;
}
