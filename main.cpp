#include "MoveConstructor/MoveConstructor.h"
#include "ConstExpr/ConstExpr.h"
#include "ExternalTemplate/ExternalTemplate.h"
#include "InitializerList/InitializerList.h"
#include "UniformInitialization/UniformInitialization.h"
#include "TypeInference/TypeInference.h"
#include "RangeBasedForLoop/RangeBasedForLoop.h"
#include "LambdaFunctions/LambdaFunctions.h"
#include "AlternativeFunctionSyntax/AlternativeFunctionSyntax.h"
#include "ObjectConstructionImprovement/ObjectConstructionImprovement.h"
#include "OverrideFinal/OverrideFinal.h"
#include "NullPointerConstant/NullPointerConstant.h"
#include "StronglyTypedEnums/StronglyTypedEnums.h"

#include <vector>

static std::vector< Module* > CreateModules()
{
	std::vector< Module* > modules{
		new MoveConstructor(),
		new ConstExpr(),
		new ExternalTemplate(),
		new InitializerList(),
		new UniformInitialization(),
		new TypeInference(),
		new RangeBasedForLoop(),
		new LambdaFunctions(),
		new AlternativeFunctionSyntax(),
		new ObjectConstructionImprovement(),
		new OverrideFinal(),
		new NullPointerConstant(),
		new StronglyTypedEnums() };
	return modules;
}

static void ExecuteModules( const std::vector< Module* >& modules,
			    int argc, char** argv )
{
	for(Module* each: modules)
		each->Main(argc, argv);
}

static void DeleteModules( std::vector< Module* >& modules )
{
	while( !modules.empty() )
	{
		Module* module = modules.back();
		modules.pop_back();
		delete module;
	}
}

int main(int argc, char* argv[])
{
	std::vector< Module* > modules = CreateModules();
	ExecuteModules( modules, argc, argv );
	DeleteModules( modules );
	return 0;
}
