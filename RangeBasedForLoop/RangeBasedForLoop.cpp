#include "RangeBasedForLoop.h"

#include <iostream>

using namespace std;

RangeBasedForLoop::RangeBasedForLoop()
	: Module("RangeBasedForLoop")
{
}

int RangeBasedForLoop::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	int myArray[]{1,2,3,4};

	cout << "Array by explicite const reference: ";
	for(int const& x: myArray)
	{
		cout << x << " ";
	}
	cout << endl;

	cout << "Array by auto type: ";
	for(auto x: myArray)
	{
		cout << x << " ";
	}
	cout << endl;


	return 0;
}

