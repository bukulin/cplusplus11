// -*- mode: c++ -*-
#ifndef RangeBasedForLoop_h
#define RangeBasedForLoop_h 1

#include "Module.h"

class RangeBasedForLoop : public Module
{
public:
	RangeBasedForLoop();
public:
	virtual int Main(int argc, char** argv) override;
};

#endif
