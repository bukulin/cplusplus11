#include "StronglyTypedEnums.h"

#include <iostream>

using namespace std;

enum class Enumeration2 : unsigned int; // Enumeration predeclaration is OK in C++11

enum class Enumeration1
{
	Val1,
	Val2,
	Val3
};

enum class Enumeration2 : unsigned int
{
	Val1 = 101,
	Val2,
	Val3 // = -1 // invalid initialization of value because of unsigned int explicit type
};

template <class C>
static ostream& enumPrintHelper(ostream& os, const C& e)
{
	os << int(e);
	return os;
}

static ostream& operator << (ostream& os, const Enumeration1& e)
{
	return enumPrintHelper(os, e);
}

static ostream& operator << (ostream& os, const Enumeration2& e)
{
	return enumPrintHelper(os, e);
}


StronglyTypedEnums::StronglyTypedEnums()
	: Module("Strongly Typed Enums")
{
}

int StronglyTypedEnums::Main(int argc, char** argv)
{
	Module::Main(argc, argv);

	cout << "Type safe enumeration:" << endl;
	cout << Enumeration1::Val1 << " "
	     << Enumeration1::Val2 << " "
	     << Enumeration1::Val3 << endl;

	// if(Enumeration1::Val1 == 101)
	// {
	// 	cout << "Type safe enumeration is not comparable with an integral value" << endl;
	// }

	cout << endl;

	cout << "Type safe enumeration with explicit type of unsigned int:" << endl;
	cout << Enumeration2::Val1 << " "
	     << Enumeration2::Val2 << " "
	     << Enumeration2::Val3 << endl;

	return 0;
}
