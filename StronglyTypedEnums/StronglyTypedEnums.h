// -*- mode: c++ -*-
#ifndef StronglyTypedEnums_h
#define StronglyTypedEnums_h 1

#include "Module.h"

class StronglyTypedEnums : public Module
{
public:
	StronglyTypedEnums();

public:
	virtual int Main(int argc, char** argv) override;
};

#endif
